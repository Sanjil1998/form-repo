<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Task-1</title>
		<link rel="stylesheet" type="text/css" href="stylesheet.css">
	</head>
	<body>
		<div class="head-section">
			<h1>Task 1</h1>
			<h3>Take two inputs then add the two inputs and also multiply the inputs as well.</h3>
		</div>
		<div class="body-section">
			<form class="form" method="post" action="#php-doc">
				<table>
				<tr>
					<td colspan="2"><label>First Input</label></td>
					<td colspan="2"><input type="number" name="number-1" id="input-1" max="99" min="1" id="inputs" value="<?php echo $_POST["number-1"]?>"></td>
				</tr>
				<tr>
					<td colspan="2"><label>Second Input</label></td>
					<td colspan="2"><input type="number" name="number-2" id="input-2" max="99" min="1" id="inputs" value="<?php echo $_POST["number-2"]?>"></td>				
				</tr>				
				<tr>
					<td><input type="submit" name="Add" value="Add"></td>					
				</tr>
				<tr>
					<td><input type="submit" name="Multiply" value="Multiply"></td>
				</tr>
				<tr>
					<td><input type="reset" name="reset"></td>					
				</tr>
				</table>
				<br>
			</form>
			<div id="php-doc">
				<?php 
				if(isset($_POST['Add'])){
					$a = $_POST["number-1"];
					$b = $_POST["number-2"];
					$c = $a + $b;
					echo $c;
				}
				else if (isset($_POST['Multiply'])){
					$a = $_POST["number-1"];
					$b = $_POST["number-2"];
					$c = $a * $b;
					echo $c;
				}
				?>
			</div>
		</div>
	</body>
</html>